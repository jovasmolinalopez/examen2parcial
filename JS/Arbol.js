class Arbol {comision
    constructor() {
        this.nodoPadre = this.agregarNodoPadre();
        this.nivel = 3;
        this.busquedaElemento;
        this.buscarNodos = [];
        this.buscarCamino="";
        this.sumaCaminos=0;
    }
    agregarNodoPadre(){
        var nodo = new Nodo(null,null,"+",0 );
        return nodo;
    }
    agregarNodo(nodoPadre,posicion,nombre,valor){
        var nodo = new Nodo(nodoPadre,posicion,nombre,valor);
        return nodo;
    }
    verificarNivelHijos(nodo){

        if(nodo.nivel == this.nivel)
            this.buscarNodos.push(nodo.nombre);

        if(nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);

        return this.buscarNodos;
    }
    buscarValor(buscarElemento,nodo){
        if(nodo.valor == buscarElemento)
            this.busquedaElemento = nodo;

        if(nodo.hasOwnProperty('hI'))
            this.buscarValor(buscarElemento,nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.buscarValor(buscarElemento,nodo.hD);

        return this.busquedaElemento;
    }
    buscarcaminoNodo(nodo){
        if(nodo.padre != null) {
            this.buscarCamino = this.buscarCamino + '' + nodo.padre.nombre;
            this.buscarcaminoNodo(nodo.padre)
        }
        return this.busquedaElemento.nombre+' '+this.buscarCamino;
    }
    //se va a sumar todos los valores hasta llegar al valor
    SumaCNodos(nodo){
        if (nodo.padre != null){
            this.sumaCaminos = this.sumaCaminos+nodo.padre.valor;
            this.SumaCNodos(nodo.padre)
        }
        return this.busquedaElemento.valor+this.sumaCaminos;
    }
}



